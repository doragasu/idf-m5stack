#include <stdio.h>
#include <lvgl.h>
#include <lvgl_helpers.h>
#include <core_i2c.h>
#include <esp_bmm150.h>
#include <mpu6886.h>
#include <led_strip.h>
#include <driver/dac.h>
#include <driver/rmt.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "util.h"

#define LV_TICK_PERIOD_MS 50
#define NUM_LEDS 10

// GPIO pins for M5Core buttons
enum {
	M5CORE_BUTTON_A = 39,
	M5CORE_BUTTON_B = 38,
	M5CORE_BUTTON_C = 37
};

#define SCR_WIDTH CONFIG_LV_HOR_RES_MAX
#define SCR_HEIGHT CONFIG_LV_VER_RES_MAX

// Module static data
static struct {
	struct {
		lv_color_t *raw_buf1;
		lv_color_t *raw_buf2;
		lv_disp_buf_t buf;
		lv_disp_drv_t drv;
		esp_timer_handle_t timer;
		struct {
			disp_backlight_h handle;
			uint_fast8_t level;
			uint_fast8_t set_level;
		} bl;
	} disp;
	struct {
		struct {
			lv_obj_t *chart;
			lv_chart_series_t *x;
			lv_chart_series_t *y;
			lv_chart_series_t *z;
		} gyr;
		struct {
			lv_obj_t *x;
			lv_obj_t *y;
			lv_obj_t *z;
			lv_style_t st_x;
			lv_style_t st_y;
			lv_style_t st_z;
		} mag;
		lv_obj_t *spin;
		lv_obj_t *batt;
	} scr;
} gui = {};

static led_strip_t *bar;

static void lv_tick_task(UNUSED void *arg)
{
	lv_tick_inc(LV_TICK_PERIOD_MS);
}

static void display_setup(void)
{
	// Initialize library and SPI interface
	lv_init();
	lvgl_driver_init();

	// Allocate double buffer
	const uint32_t buf_len = DISP_BUF_SIZE * sizeof(lv_color_t);
	gui.disp.raw_buf1 = heap_caps_malloc(2 * buf_len, MALLOC_CAP_DMA);
	assert(gui.disp.raw_buf1);
	gui.disp.raw_buf2 = gui.disp.raw_buf1 + buf_len;

	// Initialize display buffer
	const uint32_t size_in_px = DISP_BUF_SIZE;
	lv_disp_buf_init(&gui.disp.buf, gui.disp.raw_buf1, gui.disp.raw_buf2, size_in_px);

	// Initialize display driver
	lv_disp_drv_init(&gui.disp.drv);
	gui.disp.drv.flush_cb = disp_driver_flush;

	gui.disp.drv.buffer = &gui.disp.buf;
	lv_disp_drv_register(&gui.disp.drv);

	// Set up periodic GUI timer
	const esp_timer_create_args_t gui_timer_args = {
		.callback = &lv_tick_task,
		.name = "periodic_gui"
	};
	ESP_ERROR_CHECK(esp_timer_create(&gui_timer_args, &gui.disp.timer));
	ESP_ERROR_CHECK(esp_timer_start_periodic(gui.disp.timer, LV_TICK_PERIOD_MS * 1000));
}

static void led_bar_setup(void)
{
	rmt_config_t config = RMT_DEFAULT_CONFIG_TX(15, RMT_CHANNEL_0);
	// Set counter clock to 40MHz
	config.clk_div = 2;

	ESP_ERROR_CHECK(rmt_config(&config));
	ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));

	// install ws2812 driver
	led_strip_config_t strip_config = LED_STRIP_DEFAULT_CONFIG(NUM_LEDS,
			(led_strip_dev_t)config.channel);
	bar = led_strip_new_rmt_ws2812(&strip_config);
	if (!bar) {
		LOGE("install WS2812 driver failed");
	}
	// Turn off LEDs
	ESP_ERROR_CHECK(bar->clear(bar, 100));
}

static void led_bar_cycle(void)
{
	static uint32_t pos = 0;
	static const uint32_t color[NUM_LEDS][3] = {
		{ 0,  0,  0}, // Each component can go up to 255, but LEDs
		{31,  0,  0}, // can get quite hot if driven too bright
		{63,  0,  0},
		{63, 63,  0},
		{ 0, 63,  0},
		{ 0, 63, 63},
		{ 0,  0, 63},
		{ 0,  0, 31},
		{63,  0, 63},
		{63, 63, 63},
	};

	int i;
	for (i = 0; (i + pos) < NUM_LEDS; i++) {
		bar->set_pixel(bar, i, color[i + pos][0], color[i + pos][1], color[i + pos][2]);
	}
	for (int j = 0; i < NUM_LEDS; i++, j++) {
		bar->set_pixel(bar, i, color[j][0], color[j][1], color[j][2]);
	}
	bar->refresh(bar, 100);

	pos++;
	if (pos >= NUM_LEDS) {
		pos = 0;
	}
}

static void slider_create(lv_color_t color, lv_coord_t x, lv_coord_t y, lv_obj_t **slider, lv_style_t *style)
{
	*slider = lv_slider_create(lv_scr_act(), NULL);
	lv_slider_set_range(*slider, -2048, 2047);
	lv_obj_set_size(*slider, SCR_WIDTH - 100 - 10, 10);
	lv_obj_set_pos(*slider, x, y);
	lv_style_init(style);
	lv_style_set_bg_color(style, LV_STATE_DEFAULT, color);
	lv_obj_add_style(*slider, LV_SLIDER_PART_KNOB, style);
	lv_obj_add_style(*slider, LV_SLIDER_PART_INDIC, style);
}


static void gui_setup(void)
{
	// Gyroscope line graph
	gui.scr.gyr.chart = lv_chart_create(lv_scr_act(), NULL);
	lv_obj_set_size(gui.scr.gyr.chart, SCR_WIDTH, 140);
	lv_chart_set_type(gui.scr.gyr.chart, LV_CHART_TYPE_LINE);
	lv_chart_set_point_count(gui.scr.gyr.chart, 10);

	gui.scr.gyr.x = lv_chart_add_series(gui.scr.gyr.chart, LV_COLOR_MAKE(255, 0, 0));
	gui.scr.gyr.y = lv_chart_add_series(gui.scr.gyr.chart, LV_COLOR_MAKE(0, 255, 0));
	gui.scr.gyr.z = lv_chart_add_series(gui.scr.gyr.chart, LV_COLOR_MAKE(0, 0, 255));
	lv_chart_set_y_range(gui.scr.gyr.chart, LV_CHART_AXIS_PRIMARY_Y, -32768, 32767);

	// Mag sliders
	slider_create(LV_COLOR_MAKE(255, 0, 0), 10, 155, &gui.scr.mag.x, &gui.scr.mag.st_x);
	slider_create(LV_COLOR_MAKE(0, 255, 0), 10, 185, &gui.scr.mag.y, &gui.scr.mag.st_y);
	slider_create(LV_COLOR_MAKE(0, 0, 255), 10, 215, &gui.scr.mag.z, &gui.scr.mag.st_z);

	// Spinner
	gui.scr.spin = lv_spinner_create(lv_scr_act(), NULL);
	lv_spinner_set_type(gui.scr.spin, LV_SPINNER_TYPE_FILLSPIN_ARC);
	lv_spinner_set_spin_time(gui.scr.spin, 1000);
	lv_obj_set_size(gui.scr.spin, 80, 80);
	lv_obj_set_pos(gui.scr.spin, SCR_WIDTH - 90, SCR_HEIGHT - 90);

	// Battery label
	gui.scr.batt = lv_label_create(gui.scr.spin, NULL);
}

static void batt_text_set(const char *text)
{
	lv_label_set_text(gui.scr.batt, text);
	lv_obj_align(gui.scr.batt, NULL, LV_ALIGN_CENTER, 0 , 0);
}

static void dac_setup(uint_fast16_t freq)
{
	dac_cw_config_t cw_config = {
		.en_ch = DAC_CHANNEL_1,
		.scale = DAC_CW_SCALE_1,
		.phase = DAC_CW_PHASE_0,
		.freq = freq,
		.offset = 0
	};

	ESP_ERROR_CHECK(dac_cw_generator_config(&cw_config));
}

static void dac_beep(uint32_t time_ms)
{
	dac_output_enable(DAC_CHANNEL_1);
	dac_cw_generator_enable();
	vTaskDelay(pdMS_TO_TICKS(time_ms));
	dac_cw_generator_disable();
	dac_output_disable(DAC_CHANNEL_1);
}

// Battery reading function is undocumented and only supported on devices newer
// than march 2018. For more information, read:
// https://community.m5stack.com/topic/878/solved-can-t-find-ip5306-i2c-address
static const char *batt_read(void)
{
	static struct ci2c_intf pd_chip = {
		.dev_addr = 0x75
	};

	const char *level = NULL;
	uint8_t data;
	esp_err_t err = ci2c_read(0x78, &data, 1, &pd_chip);
	if (!err) {
		switch(data) {
			case 0xE0: level = "25"; break;
			case 0xC0: level = "50"; break;
			case 0x80: level = "75"; break;
			case 0x00: level = "100"; break;
			default: level = "0";
		}
	}

	return level;
}

// The GPIO handler is run in interrupt context, so be careful as calling
// blocking functions here will make the program panic.
static void IRAM_ATTR gpio_isr_handler(void* arg)
{
	gui.disp.bl.set_level += 10;
	if (gui.disp.bl.set_level > 100) {
		gui.disp.bl.set_level = 0;
	}
}

static void button_cfg(int32_t gpio_pin)
{
	const gpio_config_t io_conf = {
		.pin_bit_mask = ((uint64_t)1)<<gpio_pin,
		.mode = GPIO_MODE_INPUT,
		.intr_type = GPIO_INTR_NEGEDGE,
		.pull_down_en = 0,
		.pull_up_en = 1
	};

	//configure GPIO with the given settings
	gpio_config(&io_conf);
	//install gpio isr service
	gpio_install_isr_service(0);
	gpio_isr_handler_add(gpio_pin, gpio_isr_handler, (void*)gpio_pin);
}

static void backlight_setup(void)
{
	static const disp_backlight_config_t bl_cfg = {
		.gpio_num = 32,
		.pwm_control = true,
		.output_invert = false,
		.timer_idx = 0,
		.channel_idx = 0
	};

	gui.disp.bl.handle = disp_backlight_new(&bl_cfg);
	gui.disp.bl.level = gui.disp.bl.set_level = 30;
	disp_backlight_set(gui.disp.bl.handle, 30);
	// Setup pin to handle backlight
	button_cfg(M5CORE_BUTTON_A);
}

static void test_tsk(void *arg)
{
	int16_t x, y, z;
	struct bmm150_mag_data magf;
	struct bmm150_dev *bmm150 = bmm150_dev_get();
	bool batt_supported = true;
	uint32_t cycle = 0;

	dac_beep(100);
	vTaskDelay(pdMS_TO_TICKS(50));
	dac_beep(100);

	display_setup();
	gui_setup();
	backlight_setup();

	while (true) {
		if (gui.disp.bl.set_level != gui.disp.bl.level) {
			gui.disp.bl.level = gui.disp.bl.set_level;
			disp_backlight_set(gui.disp.bl.handle, gui.disp.bl.level);
		}
		// Get data from sensors and update GUI
		mpu6886_gyro_raw_get(&x, &y, &z);
		lv_chart_set_next(gui.scr.gyr.chart, gui.scr.gyr.x, x);
		lv_chart_set_next(gui.scr.gyr.chart, gui.scr.gyr.y, y);
		lv_chart_set_next(gui.scr.gyr.chart, gui.scr.gyr.z, z);
		LOGD("gyro: x=%d, y=%d, z=%d", x, y, z);
		const int err = bmm150_read_mag_data(&magf, bmm150);
		if (!err) {
			lv_slider_set_value(gui.scr.mag.x, magf.x, LV_ANIM_OFF);
			lv_slider_set_value(gui.scr.mag.y, magf.y, LV_ANIM_OFF);
			lv_slider_set_value(gui.scr.mag.z, magf.z, LV_ANIM_OFF);
			LOGD("magf: x=%d, y=%d, z=%d", magf.x, magf.y, magf.z);
		} else {
			LOGE("read magf error: %d", err);
		}
		led_bar_cycle();
		// Read battery once each 512 cycles
		if (batt_supported && 0 == (cycle & 0x1FF)) {
			const char *level = batt_read();
			if (!level) {
				batt_text_set("?");
				LOGW("battery read not supported");
				batt_supported = false;
			} else {
				batt_text_set(level);
				LOGD("battery level: %s", level);
			}
		}
		lv_task_handler();
		cycle++;
		vTaskDelay(pdMS_TO_TICKS(LV_TICK_PERIOD_MS));
	}
}

void app_main(void)
{
	ESP_ERROR_CHECK(ci2c_init());
	mpu6886_init();
	bmm150_configure();
	dac_setup(4000);
	led_bar_setup();

	xTaskCreatePinnedToCore(test_tsk, "gui_test", 4096, NULL, tskIDLE_PRIORITY + 1, NULL, 1);
}
